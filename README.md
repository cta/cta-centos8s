## Testing `cta-cli` in a CentOS8 container

In this directory add the following:
- `RPM` folder: unzipped artifacts from this repo
- `cta-cli.conf` file for later

Launch the container from this directory:
```
podman run -it -v ./:/shared:Z gitlab-registry.cern.ch/linuxsupport/cs8-base /bin/bash
```

Then in the container:
```
# Add missing repo for xrootd
cp /shared/repos/eos-citrine-depend.repo /etc/yum.repos.d/

# Install rpms
yum install -y /shared/RPM/RPMS/x86_64/cta-cli-4.7.11-1.el8.x86_64.rpm /shared/RPM/RPMS/x86_64/cta-lib-common-4.7.11-1.el8.x86_64.rpm

# for now use this cta-cli against a running frontend
cp /shared/etc/cta/cta-cli-preproductionfrontend.conf /etc/cta/cta-cli.conf

# install kerberos
yum install -y krb5-libs
```

Now `kinit` with you CERN account in the container:
```
[root@a1a35b01b7c2 /]# kinit <account>@CERN.CH
Password for <account>@CERN.CH: 

[root@a1a35b01b7c2 /]# cta-admin version
CTA Admin Client xrd-ssi-protobuf CTA Frontend Server xrd-ssi-protobuf Catalogue schema     DB connection string     Status 
 4.7.11-1                    v0.0     4.7.11-1                    v1.3             12.0 oracle:CTAPPS/******@cta PRODUCTION 
```
