rm cmake/Findoracle-instantclient.cmake

# Modify ABI to 1, because protobuf for centos8s is compiled with ABI=1
sed -i 's/ABI=0/ABI=1/g' CMakeLists.txt

sed -e '/(tapeserver/ s/^#*/#/' -i CMakeLists.txt
sed -e '/tory(tests/ s/^#*/#/' -i CMakeLists.txt
sed -e '/cta-release/ s/^#*/#/' -i CMakeLists.txt
